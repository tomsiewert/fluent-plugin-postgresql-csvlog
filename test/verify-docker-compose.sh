#!/bin/sh

# This script is use by the end-to-end fluent test
# See the docker-compose.yml for more details

cleanup() {
  echo "# removing all logs"
  find /var/log/pg/ -name "pg_stat_statements.*.log" -delete
  find /var/log/pg/ -name "pg_stat_activity.*.log" -delete
}

die() {
  cleanup
  echo "$1"
  exit 1
}

cleanup
echo "# sleeping 10, awaiting logs"
sleep 10;

echo "# looking for pg_stat_statements"

(find /var/log/pg/ -name "pg_stat_statements.*.log" | grep .  >/dev/null) || die "No pg_stat_statements files created"
cat /var/log/pg/pg_stat_statements.*.log | tail -10

echo "# looking for pg_stat_activity"

(find /var/log/pg/ -name "pg_stat_activity.*.log" | grep . >/dev/null) || die "No pg_stat_activity files created"
cat /var/log/pg/pg_stat_activity.*.log | tail -10

cleanup
