# frozen_string_literal: true

require 'fluent/plugin/filter'
require_relative './marginalia_extractor'

module Fluent
  module Plugin
    # Filters SQL statements for Marginalia comments.
    #
    # Examples:
    # SELECT COUNT(*) FROM "projects" /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/
    # /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/ SELECT COUNT(*) FROM "projects"
    #
    class Marginalia < Filter
      include MarginaliaExtractor
      Fluent::Plugin.register_filter('marginalia', self)

      desc 'Field to parse for Marginalia comments (key1:value1,key2:value2)'
      config_param :key, :string, default: 'sql'

      desc 'Whether to strip the comment from the record specified by key'
      config_param :strip_comment, :bool, default: true

      def filter(_tag, _time, record)
        parse_marginalia_into_record(record, @key, @strip_comment)

        record
      end
    end
  end
end
