# frozen_string_literal: true

require 'fluent/plugin/filter'

module Fluent::Plugin
  # MarginaliaExtractor provides the parse_marginalia_into_record
  # utility method, useful for extracting marginalia into fluentd records
  module MarginaliaExtractor
    MARGINALIA_PREPENDED_REGEXP = %r{^(?<comment>/\*.*?\*/)\s*(?<sql>.*)}m
    MARGINALIA_APPENDED_REGEXP = %r{(?<sql>.*)(?<comment>/\*.*\*/)\s*;?\s*$}m

    # Injects marginalia into a fluentd record
    def parse_marginalia_into_record(record, key, strip_comment)
      sql = record[key]
      return unless sql

      comment_match = match_marginalia_comment(sql)

      return unless comment_match

      entries = extract_entries(comment_match['comment'])
      parse_entries(entries, key, record)

      record[key] = comment_match['sql'].strip if strip_comment
    end

    def match_marginalia_comment(sql)
      matched = MARGINALIA_PREPENDED_REGEXP.match(sql)

      return matched if matched

      MARGINALIA_APPENDED_REGEXP.match(sql)
    end

    def extract_entries(comment)
      comment = scrub_comment(comment)

      return [] unless comment

      comment.split(',')
    end

    def scrub_comment(comment)
      return unless comment

      comment.strip!
      comment.gsub!(%r{^/\*}, '')
      comment.gsub!(%r{\*/$}, '')
    end

    def parse_entries(entries, key, record)
      entries.each do |component|
        data = component.split(':', 2)

        break unless data.length == 2

        stored_key = store_key(record, key, data[0])
        record[stored_key] = data[1]
      end
    end

    def store_key(record, key, component_key)
      # In case there is a conflict with the Marginalia key
      # (e.g. `correlation_id`), we use the base key
      # (`sql_correlation_id`) instead.
      if record.key?(component_key)
        "#{key}_#{component_key}"
      else
        component_key
      end
    end
  end
end
