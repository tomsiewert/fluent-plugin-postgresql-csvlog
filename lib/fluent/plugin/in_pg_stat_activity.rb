# frozen_string_literal: true

require_relative './polling_pg_input_plugin'
require 'pg_query'
require_relative './marginalia_extractor'
require 'time'

module Fluent::Plugin
  # PgStatActivityInput polls the `pg_stat_activity` table
  # emitting normalized versions of the queries currently running on
  # the postgres server.
  # Fingerprints of the queries are also included for easier aggregation
  class PgStatActivityInput < PollingPostgresInputPlugin
    include MarginaliaExtractor
    Fluent::Plugin.register_input('pg_stat_activity', self)

    ACTIVITY_QUERY = <<-SQL
      SELECT
        datid,
        datname,
        pid,
        usesysid,
        usename,
        application_name,
        host(client_addr) as client_addr,
        client_hostname,
        client_port,
        wait_event_type,
        wait_event,
        xact_start,
        CAST(extract(epoch from clock_timestamp() - xact_start) AS double precision) xact_age_s,
        query_start,
        CAST(extract(epoch from clock_timestamp() - query_start) AS double precision) query_age_s,
        state_change,
        CAST(extract(epoch from clock_timestamp() - state_change) AS double precision) state_age_s,
        state,
        query
      FROM pg_catalog.pg_stat_activity
      WHERE usename IS NOT NULL
    SQL

    desc 'Name of field to store SQL query fingerprint'
    config_param :fingerprint_key, :string, default: 'fingerprint'

    protected

    def on_poll
      with_connection do |conn|
        emit_activity_to_stream(conn)
      end
    end

    public

    # Query the database and emit statements to fluentd router
    def emit_activity_to_stream(conn)
      me = Fluent::MultiEventStream.new

      now = Fluent::Engine.now
      conn.exec(ACTIVITY_QUERY).each do |row|
        record = record_for_row(row)
        me.add(now, record)
      end

      @router.emit_stream(@tag, me)
    end

    # Returns a fluentd record for a query row
    def record_for_row(row)
      record = {
        'datid' => row['datid'],
        'datname' => row['datname'],
        'pid' => row['pid'],
        'usesysid' => row['usesysid'],
        'usename' => row['usename'],
        'application_name' => row['application_name'],
        'client_addr' => row['client_addr'],
        'client_hostname' => row['client_hostname'],
        'client_port' => row['client_port'],
        'wait_event_type' => row['wait_event_type'],
        'wait_event' => row['wait_event'],
        'xact_start' => row['xact_start']&.iso8601(3),
        'xact_age_s' => row['xact_age_s'],
        'query_start' => row['query_start']&.iso8601(3),
        'query_age_s' => row['query_age_s'],
        'state_change' => row['state_change']&.iso8601(3),
        'state_age_s' => row['state_age_s'],
        'state' => row['state'],
        'query' => row['query'] # This will be stripped, normalized etc
      }

      # Inject marginalia into record
      parse_marginalia_into_record(record, 'query', true)

      # Normalize query and fingerprint
      # Note that `record['query']` was updated in previous step
      # To strip off marginalia comments
      record.merge!(fingerprint_query(record['query']))

      record
    end

    def fingerprint_query(query)
      # We record the query_length as it will help in understanding whether unparseable
      # queries are truncated.
      record = { 'query_length' => query&.length, 'query' => nil }

      return record unless query

      normalized = PgQuery.normalize(query)
      record['query'] = normalized

      record[@fingerprint_key] = PgQuery.parse(normalized).fingerprint if @fingerprint_key

      record
    rescue PgQuery::ParseError
      record['query_unparseable'] = true

      record
    end
  end
end
